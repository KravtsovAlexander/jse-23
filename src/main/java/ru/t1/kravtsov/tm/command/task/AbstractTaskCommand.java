package ru.t1.kravtsov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.service.IProjectTaskService;
import ru.t1.kravtsov.tm.api.service.ITaskService;
import ru.t1.kravtsov.tm.command.AbstractCommand;
import ru.t1.kravtsov.tm.enumerated.Role;
import ru.t1.kravtsov.tm.enumerated.Status;
import ru.t1.kravtsov.tm.model.Task;

import java.util.List;

public abstract class AbstractTaskCommand extends AbstractCommand {

    @NotNull
    protected IProjectTaskService getProjectTaskService() {
        return getServiceLocator().getProjectTaskService();
    }

    @NotNull
    protected ITaskService getTaskService() {
        return getServiceLocator().getTaskService();
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    protected void displayTask(@Nullable final Task task) {
        if (task == null) return;
        System.out.println("ID: " + task.getId());
        System.out.println("NAME: " + task.getName());
        System.out.println("DESCRIPTION: " + task.getDescription());
        System.out.println("STATUS: " + Status.toName(task.getStatus()));
    }

    protected void renderTasks(@NotNull final List<Task> tasks) {
        int index = 1;
        for (final Task task : tasks) {
            @NotNull final String name = task.getName();
            @NotNull final String description = task.getDescription();
            @NotNull final String id = task.getId();
            @Nullable final String status = Status.toName(task.getStatus());
            System.out.printf("%s. (%s) %s : %s : %s\n", index, id, name, description, status);
            index++;
        }
    }

    @NotNull
    @Override
    public Role[] getRoles() {
        return Role.values();
    }

}
