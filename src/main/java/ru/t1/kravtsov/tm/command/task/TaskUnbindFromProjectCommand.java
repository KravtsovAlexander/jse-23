package ru.t1.kravtsov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.kravtsov.tm.util.TerminalUtil;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @NotNull
    public static final String DESCRIPTION = "Unbind task from project.";

    @NotNull
    public static final String NAME = "task-unbind-from-project";

    @Override
    public void execute() {
        System.out.println("[UNBIND TASK FROM PROJECT]");
        System.out.println("ENTER PROJECT ID:");
        @NotNull final String projectID = TerminalUtil.nextLine();
        System.out.println("ENTER TASK ID:");
        @NotNull final String taskID = TerminalUtil.nextLine();
        getProjectTaskService().unbindTaskFromProject(getUserId(), projectID, taskID);
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
