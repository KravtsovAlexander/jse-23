package ru.t1.kravtsov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.kravtsov.tm.api.repository.IRepository;
import ru.t1.kravtsov.tm.api.service.IService;
import ru.t1.kravtsov.tm.enumerated.Sort;
import ru.t1.kravtsov.tm.exception.entity.EntityNotFoundException;
import ru.t1.kravtsov.tm.exception.field.IdEmptyException;
import ru.t1.kravtsov.tm.exception.field.IndexIncorrectException;
import ru.t1.kravtsov.tm.model.AbstractModel;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractService<M extends AbstractModel, R extends IRepository<M>> implements IService<M> {

    @NotNull
    protected final R repository;

    public AbstractService(final @NotNull R repository) {
        this.repository = repository;
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @NotNull
    @Override
    public List<M> findAll() {
        return repository.findAll();
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final Comparator<M> comparator) {
        if (comparator == null) return findAll();
        return repository.findAll(comparator);
    }

    @NotNull
    @Override
    @SuppressWarnings("unchecked")
    public List<M> findAll(@Nullable final Sort sort) {
        if (sort == null) return findAll();
        @Nullable final Comparator<M> comparator = sort.getComparator();
        return findAll(comparator);
    }

    @NotNull
    @Override
    public M add(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.add(model);
    }

    @Override
    public void deleteAll() {
        repository.deleteAll();
    }

    @Override
    public void deleteAll(@NotNull final List<M> models) {
        repository.deleteAll(models);
    }

    @NotNull
    @Override
    public M findOneById(final @NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.findOneById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M findOneByIndex(final @NotNull Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        @Nullable final M model = repository.findOneByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Nullable
    @Override
    public M remove(@Nullable final M model) {
        if (model == null) throw new EntityNotFoundException();
        return repository.remove(model);
    }

    @NotNull
    @Override
    public M removeById(final @NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        @Nullable final M model = repository.removeById(id);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @NotNull
    @Override
    public M removeByIndex(final @NotNull Integer index) {
        if (index < 0) throw new IndexIncorrectException();
        @Nullable final M model = repository.removeByIndex(index);
        if (model == null) throw new EntityNotFoundException();
        return model;
    }

    @Override
    public boolean existsById(final @NotNull String id) {
        if (id.isEmpty()) throw new IdEmptyException();
        return repository.existsById(id);
    }

    @Override
    public int getSize() {
        return repository.getSize();
    }

}
